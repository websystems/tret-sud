$(function() {
    // collapse list
    $('.collapseList').on('click', 'a', function(e) {
        e.preventDefault();
        $(this).closest('li').find('p').slideToggle();
    });

    var width = $(window).width();
    function leftMenu() {
            $('.left-menu').on('click', 'span', function(e) {
                if (width < 992) {
                    e.preventDefault();
                    $(this).closest('li').find('.hover-drop').slideToggle(100);
                }
            });
    }

    leftMenu();

    function footer() {
        var height = $(document).height();
        $('body').css('height', height);
    }

    footer();

    $(window).resize(function() {
        leftMenu();
        footer();
    });

    $(window).load(function() {
        $("#loaderInner").fadeOut();
        $("#loader").delay(400).fadeOut("slow");
    });
});



